using tech_test_payment_api.Controllers;
using Microsoft.AspNetCore.TestHost;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Xunit.Abstractions;

namespace tech_test_payment_api.Tests;

public class UnitTesteVendas
{
    // Fontes:
    // https://learn.microsoft.com/pt-br/dotnet/core/testing/unit-testing-with-dotnet-test
    // https://www.learmoreseekmore.com/2022/02/dotnet6-unit-testing-aspnetcore-web-api-using-xunit.html
    // https://learn.microsoft.com/pt-br/dotnet/architecture/microservices/multi-container-microservice-net-applications/test-aspnet-core-services-web-apps
    // https://code-maze.com/unit-testing-aspnetcore-web-api/

    private readonly ITestOutputHelper output;
    public UnitTesteVendas(ITestOutputHelper output){
        Models.Repositorio.AdicionaVendedoresTeste();
        this.output = output;
    }

    [Fact]
    public void TesteRegistrarVenda() {
        var controller = new VendaController();

        var vendaTeste = new Models.Venda() {IdVendedor = 0, Itens = new List<string>() {"Bola", "Luva"}};

        // Verifica se o resultado retorna Ok (status 200)
        var res = controller.RegistrarVenda(vendaTeste);
        Assert.IsType<OkObjectResult>(res as OkObjectResult); 
    }

    [Fact]
    public void TesteBuscarVenda() {
        var controller = new VendaController();

        // Verifica se o resultado retorna Ok (status 200)
        var res = controller.BuscarVenda(0, false) as OkObjectResult;
        Assert.IsType<OkObjectResult>(res);

        // Verifica se a venda obtida é realmente a que foi requisitada
        var v = Assert.IsType<Models.Venda>(res.Value);
        Assert.Equal(0, v.Id);
    }


    [Fact]
    public void TesteAtualizarVenda() {
        var controller = new VendaController();

        var vendaTeste = new Models.Venda() {IdVendedor = 0, Itens = new List<string>() {"Bola", "Luva"}};
        controller.RegistrarVenda(vendaTeste);
        
        // ***************** Testar operações não permitidas *****************
        var res = controller.AtualizarVenda(0, Models.EnumStatusVenda.Enviado_para_transportadora);
        Assert.IsType<ConflictObjectResult>(res as ConflictObjectResult); 

        res = controller.AtualizarVenda(0, Models.EnumStatusVenda.Entregue);
        Assert.IsType<ConflictObjectResult>(res as ConflictObjectResult); 

        // ***************** Testar operações permitidas *****************
        res = controller.AtualizarVenda(0, Models.EnumStatusVenda.Pagamento_aprovado);
        Assert.IsType<OkObjectResult>(res as OkObjectResult); 

        res = controller.AtualizarVenda(0, Models.EnumStatusVenda.Enviado_para_transportadora);
        Assert.IsType<OkObjectResult>(res as OkObjectResult); 

        res = controller.AtualizarVenda(0, Models.EnumStatusVenda.Entregue);
        Assert.IsType<OkObjectResult>(res as OkObjectResult);         
    }
}