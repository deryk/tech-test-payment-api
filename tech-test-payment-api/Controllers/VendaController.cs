using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers;

/// <summary>
/// Controller responsável pelas operações de Venda
/// </summary>
[ApiController]
[Route("api/[controller]")]
public class VendaController : ControllerBase
{    
    /// <summary>
    /// Registra venda
    /// </summary>
    /// <response code="200">Venda adicionada com sucesso</response>
    /// <response code="400">Vendedor não cadastrado</response>
    [HttpPost("RegistrarVenda")]
    public IActionResult RegistrarVenda(Venda venda)
    {
        var vendedor = Repositorio.Vendedores.Find(v => v.Id == venda.IdVendedor);
        if (vendedor == null)
            return this.BadRequest("Vendedor inválido!");

        venda.Id = Repositorio.Vendas.Count;
        Repositorio.Vendas.Add(venda);
        return Ok(venda);
    }

    /// <summary>
    /// Buscar venda pelo ID
    /// </summary>
    /// <returns>Retorna a venda</returns>
    /// <response code="200">Retorna a venda encontrada com os dados do vendedor</response>
    /// <response code="404">Venda não encontrada</response>
    [HttpGet("BuscarVenda")]
    public IActionResult BuscarVenda(int? id, bool ComVendedor = true)
    {
        if (id == null)
            return NotFound("É necessário um Id válido!");

        var venda = Repositorio.Vendas.Find(v => v.Id == id);

        if (venda == null)
            return NotFound("Venda não encontrada!");

        if (!ComVendedor)
            return Ok(venda);

        var vendedor = Repositorio.Vendedores.Find(v => v.Id == venda.IdVendedor);
        return Ok(new {venda, vendedor});
    }

    /// <summary>
    /// Atualiza o status da situação da venda
    /// </summary>
    /// <response code="200">Atualização feita com sucesso</response>
    /// <response code="404">Venda não encontrada</response>
    /// <response code="409">Operação não permitida</response>
    [HttpPatch("AtualizarVenda")]
    public IActionResult AtualizarVenda(int id, EnumStatusVenda status)
    {
        var venda = Repositorio.Vendas.Find(v => v.Id == id);

        if (venda == null)
            return NotFound("Venda não encontrada!");

        var operacoesPermitidas = new EnumStatusVenda[][] {
                                    new EnumStatusVenda[] {EnumStatusVenda.Aguardando_pagamento, EnumStatusVenda.Pagamento_aprovado},
                                    new EnumStatusVenda[] {EnumStatusVenda.Aguardando_pagamento, EnumStatusVenda.Cancelada},
                                    new EnumStatusVenda[] {EnumStatusVenda.Pagamento_aprovado, EnumStatusVenda.Enviado_para_transportadora},
                                    new EnumStatusVenda[] {EnumStatusVenda.Pagamento_aprovado, EnumStatusVenda.Cancelada},
                                    new EnumStatusVenda[] {EnumStatusVenda.Enviado_para_transportadora, EnumStatusVenda.Entregue},
                                };
        
        if (!operacoesPermitidas.Any( s => s[0] == venda.StatusVenda && s[1] == status))
            return this.Conflict("Operação não permitida!");

        venda.StatusVenda = status;
                
        return Ok(new { msg = $"Status da Venda {venda.Id} atualizado para '{venda.StatusVenda.ToString()}'"});
    }
}
