
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(opt =>
{
    opt.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "TodoAPI", Version = "v1" });

    var xmlFile = $"{System.Reflection.Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    
    opt.IncludeXmlComments(xmlPath);
});

var app = builder.Build();

// Caso a documentação esteja disponível somente em ambiente de execução
if (app.Environment.IsDevelopment())
{
    app.UseSwagger(c => { c.RouteTemplate = "api-docs/{documentName}/swagger.json"; });
    app.UseSwaggerUI(c =>
    {
        //c.SwaggerEndpoint("../swagger/v1/swagger.json", "APP API - V1"); // Caso a rota seja em outro local
        c.RoutePrefix = "api-docs";
    });

}

tech_test_payment_api.Models.Repositorio.AdicionaVendedoresTeste();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.UseRouter(rbuilder =>
{
    // Rotear caminho /swagger para /api-docs
    rbuilder.MapGet("/swagger", context =>
    {
        context.Response.Redirect("/api-docs");
        return Task.FromResult(0);
    });
});

app.Run();