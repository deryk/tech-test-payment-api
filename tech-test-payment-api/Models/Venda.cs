namespace tech_test_payment_api.Models
{
    public class Venda : Registro
    {
        public int IdVendedor { get; set; }
        public List<string> Itens { get; set; }

        public EnumStatusVenda StatusVenda {get ; set; }

    }
}