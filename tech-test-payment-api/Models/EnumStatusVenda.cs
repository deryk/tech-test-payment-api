namespace tech_test_payment_api.Models;

/// <summary>
/// Aguardando_pagamento = 0, Pagamento_aprovado = 1, Enviado_para_transportadora = 2, Entregue = 3, Cancelada = 4
/// </summary>
public enum EnumStatusVenda
{
    Aguardando_pagamento = 0,
    Pagamento_aprovado = 1,
    Enviado_para_transportadora = 2,
    Entregue = 3,
    Cancelada = 4
}



