namespace tech_test_payment_api.Models;
public static class Repositorio {
    public static List<Vendedor> Vendedores = new List<Vendedor>();
    public static List<Venda> Vendas = new List<Venda>();

    public static void AdicionaVendedoresTeste() {
        Vendedores.AddRange( new Vendedor[] {
                                new Vendedor() {Id= 0, Nome = "Fulano", Cpf = "999.999.999-99",
                                Email = "fulano@email.com", Telefone = "(11) 9456-4567"},

                                new Vendedor() {Id= 1, Nome = "Ciclano", Cpf = "111.111.111-11",
                                Email = "ciclano@email.com", Telefone = "(11) 3333-3333"},
                            });
    }    
}